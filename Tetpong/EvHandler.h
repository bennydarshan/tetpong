#pragma once
#include <list>
#include <string>

#include "Subscriber.h"

class EvHandler {
private:
	std::list<Subscriber *> subs[27];
public:
	EvHandler(){}
	void Subscribe(const std::string keys, Subscriber *sub)
	{
		for (auto key : keys)
		{
			if (key == 27) subs[27 - 1].emplace_back(sub);
			else subs[key - 'a'].emplace_back(sub);

		}
	}
	bool Poll();
};