#pragma once
#include "Paddle.h"
#include "Ball.h"

class PaddleAI : public Paddle {
private:
	int difficult;
public:
	PaddleAI(int x, int y, char ch, int diff): Paddle(x, y, ch), difficult(diff){}
	void process(const Ball &ball) {
		if (!difficult) return;
		Point::dir d = Point::DOWN;
		if (this->y < ball.getY())
		{
			d = Point::DOWN;
		}
		else if (this->y > ball.getY())
		{
			d = Point::UP;
		}
		auto precent = rand() % 40;
		if((difficult == 1) && (ball.isbomb())) d = d == Point::UP ? Point::DOWN : Point::UP;
		if ((difficult == 2) && (precent == 1))  d = d == Point::UP ? Point::DOWN : Point::UP;
		else if((difficult == 3) && (precent < 4)) d = d == Point::UP ? Point::DOWN : Point::UP;
		move(d);
	}
};