#pragma once

#include "Point.h"

class Wall {

public:
	enum side { LEFT, RIGHT };
	Wall(side set) : s(set){};
	void draw();
	void embed(int y);
	int query();
	void shift(int x);
	int bombhit(const Point &bomb);
private:
	side s;
	const char ch = '#';
	bool blocks[21][20] = { false };

};