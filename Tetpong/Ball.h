#pragma once

#include<array>

#include "Point.h"
#include "Subscriber.h"

class Ball : public Point, public Subscriber {
private:
	int stlx, stly;
	const char sch;
	const char wch;
	Point::dir dx, dy;
	int minx, maxx;
	int bombstate = 0;
	const char lbmb, rbmb;
	void invertY() {
		dy = dy == Point::UP ? Point::DOWN : Point::UP;
	}
	void earse();
public:
	Ball(int tlx, int tly, char ch, char leftbomb, char rightbomb);
	void restbomb() {
		if ((bombstate <= 9) && (bombstate >= -9))
			bombstate = 0;
	}
	void draw() const;
	void handle(char ev) {
		if (ev == 's') // check to see if the bomb circled around the x=40 4 times;
		{
			if (bombstate < 160) return;
		}
		else {
			if (bombstate > -160) return;
		}
		if (bombstate != 0) return;
		if (ev == lbmb) {
			bombstate--;
		}
		else if (ev == rbmb) {
			bombstate++;
		}
	}
	std::string bindings() {
	 std::string ret;
	 ret = "";
	 ret += lbmb;
	 ret += rbmb;
	 return ret;
	}
	void reset();
	bool move();
	bool hit(Point &obj);
	void invertX() { dx == Point::LEFT ? dx = Point::RIGHT : dx = Point::LEFT; }
	Point::dir DirX() { return dx; }
	void setmX(int x) {
		minx += x;
	}
	void setMX(int x) {
		maxx -= x;
	}
	bool isbomb() const{
		return abs(bombstate) >= 10;
	}
};