#pragma once

#include "Paddle.h"
#include "Subscriber.h"

class PaddlePlr : public Paddle, public Subscriber {
private:
	char kup, kdown;
public:
	PaddlePlr(int x, int y, char ch, char kup, char kdn): Paddle(x, y, ch), kup(kup), kdown(kdn) {}
	void handle(char ev)
	{
		if (ev == kup) move(Point::UP);
		else if (ev == kdown) move(Point::DOWN);
	}
	std::string bindings()
	{
		std::string ret = "";
		ret += kup;
		ret += kdown;
		return ret;
	}
};