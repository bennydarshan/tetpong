#pragma once

#include "Utils.h"
#include <iostream>
#include <string>

//This will be the base object for all of the drawables in the game.

class Point {
private:
	// shrug
public:
	enum dir { UP, DOWN, LEFT, RIGHT };
	Point(int x, int y, int w, int h, char chs) : x(x), y(y), w(w), h(h), chs(chs) {}


protected:
	int x, y;
	int w, h;
	char chs;
	bool canmove(dir d, int minx, int maxx)
	{
		switch (d) {
		case LEFT:
			if (x == minx) return false;
			break;
		case RIGHT:
			if (x + w == maxx) return false;
			break;
		case UP:
			if (y == MIN_Y) return false;
			break;
		case DOWN:
			if (y + h == MAX_Y) return false;
			break;
		}
		//never get here
		return true;
	}
	bool canmove(dir d) {
		return canmove(d, MIN_X, MAX_X);
	}
	
public:
	virtual void draw() const {}
	int getX() const { return x; }
	int getY() const { return y; }
	bool hitdetect(const Point &obj)
	{
		if ((x + w >= obj.x) &&
			(y <= obj.y + obj.h) &&
			(x <= obj.x + obj.w) &&
			(y + h >= obj.y)) return true;
		return false;
	}
	//virtual bool move() = 0;
};