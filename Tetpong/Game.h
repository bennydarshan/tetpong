#pragma once

#include "Paddle.h"
#include "PaddleAi.h"
#include "PaddlePlr.h"
#include "Ball.h"
#include "EvHandler.h"
#include "Wall.h"
#include "Subscriber.h"

class Game : public Subscriber{
private:
	bool running = true;
	const char exit = 27;
	int gdiff1, gdiff2;
	EvHandler ev;
	PaddlePlr p1;
	PaddlePlr p2;
	PaddleAI a1;
	PaddleAI a2;
	Wall w1;
	Wall w2;
	Ball b1;
public:
	Game(int diff1, int diff2) : p1(4, 10, '#', 'q', 'a'), p2(76, 11, '#', 'p', 'l'),a1(4,10,'#', diff1),a2(76,11, '#', diff2), b1(40, 10, 'O', 's', 'k'), w1(Wall::LEFT),w2(Wall::RIGHT), gdiff1(diff1), gdiff2(diff2) {
		if(!diff1) ev.Subscribe(p1.bindings(), &p1);
		if(!diff2) ev.Subscribe(p2.bindings(), &p2);
		ev.Subscribe(b1.bindings(), &b1);
		ev.Subscribe(this->bindings(), this);
	};
	Game() :Game(0,0) { }
	void GameStart() 
	{ 
		running = true;
		system("cls");
		if(!gdiff1) p1.draw();
		else a1.draw();
		if(!gdiff2) p2.draw();
		else a2.draw();
		w1.draw();
		w2.draw();
		b1.draw();
	}
	void GameLoop();
	std::string bindings() {
		std::string ret = "";
		ret += exit;
		return ret;
	}
	void handle(char ev)
	{
		running = 0;
	}
};