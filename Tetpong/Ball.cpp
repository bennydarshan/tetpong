#include "Ball.h"
#include <iostream>

void Ball::earse()
{
	for (int i = 0; i < h; i++)
	{
		gotoxy(x, y + i);
		std::cout << "    ";
	}
}

Ball::Ball(int tlx, int tly, char ch, char leftbomb, char rightbomb) : Point(tlx, tly, 4, 3, ch), dx(Point::RIGHT), dy(Point::DOWN), stlx(tlx), stly(tly), sch('@'), wch('!'), minx(MIN_X), maxx(MAX_X), lbmb(leftbomb), rbmb(rightbomb)
{
	
}

void Ball::draw() const
{
	char chs;
	if (!bombstate) chs = this->chs;
	else if ((bombstate <= 9) && (bombstate >= -9))
		char chs = wch;
	else chs = this->sch;
	gotoxy(x, y);
	std::cout << ' ' << chs << chs << ' ';
	gotoxy(x, y + 1);
	std::cout << chs << chs << chs << chs;
	gotoxy(x, y + 2);
	std::cout << ' ' << chs << chs << ' ';
}

void Ball::reset()
{
	earse();
	//if (x >= 40) maxx--;
	//else minx++;
	x = stlx;
	y = stly;
	invertX();
	draw();
}

bool Ball::move()
{
	if (!bombstate) {
		if (!canmove(dx, minx, maxx)) return false;
	}
	else
		if (!canmove(dx)) return false;
	if (!canmove(dy)) invertY();
	earse();
	dx == Point::RIGHT ? x++ : x--;
	dy == Point::DOWN ? y++ : y--;
	draw();
	if (bombstate > 0) bombstate++;
	else if (bombstate < 0) bombstate--;
	return true;
}

bool Ball::hit(Point &obj)
{
	if ((obj.getX() < 40) && (dx == Point::RIGHT)) return false;
	else if ((obj.getX() > 40) && (dx == Point::LEFT)) return false;
	return hitdetect(obj);
}
