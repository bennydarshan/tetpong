#include "Paddle.h"
#include<iostream>

void Paddle::draw() const
{
	for (int i = 0; i < h; i++)
	{
		gotoxy(x, y + i);
		std::cout << chs;
	}
}

bool Paddle::move(Point::dir d)
{
	if (!canmove(d)) return false;
	switch (d) {
	case UP:
		y--;
		gotoxy(x, y);
		std::cout << chs;
		gotoxy(x, y + h);
		std::cout << ' ';
		break;
	case DOWN:
		gotoxy(x, y);
		std::cout << ' ';
		
		gotoxy(x, y + h);
		std::cout << chs;
		y++;
		break;
	case LEFT:
		x--;
		for (int i = 0; i < h; i++)
		{
			gotoxy(x, y + i);
			std::cout << chs << ' ';
		}
		break;
	case RIGHT:
		
		for (int i = 0; i < h; i++)
		{
			gotoxy(x, y + i);
			std::cout << ' ' << chs;
		}
		x++;
		break;

	}
	return true;
}



void Paddle::die()
{
	const int mid = 40;
		bool left = x < 40;
		Point::dir change;
		Point::dir animate;
		if (!left) {
			change = Point::LEFT;
			animate = Point::RIGHT;
		}
		else {
			change = Point::RIGHT;
			animate = Point::LEFT;
		}
		//TODO: implement animation;
		deaths++;
		move(change);
}

void Paddle::heal(int health)
{
	deaths -= health;
	const int mid = 40;
	bool left = x < 40;
	Point::dir change;
	Point::dir animate;
	if (!left) {
		change = Point::RIGHT;
	}
	else {
		change = Point::LEFT;
	}
	for (int i = 0; i < health; i++)
		move(change);
}
