#include "Wall.h"

#include<iostream>

// due to the change, wall is too specific to inherit from anyone, so it will just call gotoxy

void Wall::draw()
{
	for(int i=0;i<21;i++)
		for (int j = 0; j < 20; j++) {
				int x = j;
				if (s == RIGHT) x = MAX_X - j;
				gotoxy(x, i + MIN_Y);
				if(blocks[i][j]) (std::cout << ch);
		}
}

void Wall::embed(int y)
{
	int column = 0;
	int y2, y3;
	y2 = y + 1;
	y3 = y2 + 1;
	for (int i = 1; i < 20; i++)
	{
		if (blocks[y][i - 1]) column = i;
		if (blocks[y2][i - 1]) column = i;
		if (blocks[y3][i - 1]) column = i;
	}
	blocks[y][column] = true;
	blocks[y2][column] = true;
	blocks[y3][column] = true;
	draw();
}

int Wall::query()
{
	for (int i = 0; i < 20; i++)
	{
		bool flag = true;
		for (int j = 0; j < 21; j++)
		{
			
			if (!blocks[j][i]) flag = false;
		}
		if (flag) return i;
	}
	return -1;
}

void Wall::shift(int x)
{
	int offset = (s == LEFT) ? (MIN_X + x) : (MAX_X - x);
	gotoxy(offset, MIN_Y);
	for (int j = 0; j < 21; j++)
	{
		std::cout << ' ';
		blocks[j][x] = false;
		gotoxy(++offset, MIN_Y);
	}
	for (int i = x + 1; i < 20; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			if (blocks[j][i]) {
				blocks[j][i] = false;
				blocks[j][i - 1] = true;
				offset = (s == LEFT) ? (MIN_X + i) : (MAX_X - i);
				gotoxy(offset, MIN_Y + j);
				std::cout << ' ';
				auto change = s == LEFT ? -1 : 1;
				gotoxy(offset + change, MIN_Y + j);
				std::cout << ch;
				//adjuster.move(change);
				//adjuster.draw();
			}
		}
	}
	draw();
}

int Wall::bombhit(const Point & bomb)
{
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 21; j++) {
			auto offset = (s == LEFT) ? (MIN_X + i) : (MAX_X - i);
			Point temp(offset, MIN_Y + j, 1, 1, '#');
			if (temp.hitdetect(bomb)) return i;
		}
	}
	return -1;
}
