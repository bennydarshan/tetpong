#pragma once

#include<string>

class Subscriber {
public:
	virtual void handle(char ev) = 0;
	virtual std::string bindings() = 0;
};