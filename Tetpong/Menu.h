#pragma once

#include "Game.h"
#include "Utils.h"
#include <iostream>
#include <conio.h>

class Menu {
private:
	Game *g = nullptr;
	int selection;
	int diff1 = 0, diff2 = 0;
	const int x = 40, y = 10;
	const int entriesc = 6;
	const std::string entries[6] = { "(1) Start a new game - Human vs Human", "(2) Start a new game - Human vs Computer","(3) Start a new game - Computer vs Computer", "(4) Continue a paused game", "(8) Present instructions and keys", "(9) EXIT" };
public:
	Menu() : selection(-1) {};
	~Menu() {
		if (g) delete g;
		g = nullptr;
	}
	int show() {
		if(g) system("cls");
		for (int i = 0; i < entriesc; i++) {
			gotoxy(x, y + i);
			std::cout << entries[i];
		}
		while (!_kbhit());
		selection = _getch() - '0';
		if ((selection < 1) && (selection > 4) && (selection != 8) && (selection != 9)) selection = -1;
		return selection;
	}
	void help() {
		int yoffset = 0;
		gotoxy(x-8, y + yoffset++);
		std::cout << "Player one uses q and a, Player two uses p and l";
		gotoxy(x-8, y + yoffset++);
		std::cout << "Your objective is to score the other player, keeping your gate safe";
		gotoxy(x-8, y + yoffset++);
		std::cout << "The wall behind you fills up whenever you die, complete a column to get hp back.";
		gotoxy(x-8, y + yoffset++);
		gotoxy(x-8, y + yoffset++);
		std::cout << "Press any key to return to the main menu";
		selection = -1;
		while (!_kbhit());
		system("cls");
	}
	void getdiff(int x) {
		system("cls");
		gotoxy(x, y);
		std::cout << "Please enter diffculty for CPU" << x;
		gotoxy(x, y+1);
		std::cout << "(a) Best (b) Good or (c) Novice";
		while (!_kbhit());
		auto difftemp = tolower(_getch());
		if ((difftemp != 'a') && (difftemp != 'b') && (difftemp != 'c')) getdiff(x);
		if (x == 1) diff1 = difftemp - 'a' + 1;
		else diff2 = difftemp - 'a' + 1;
	}
	void act() {
		switch (selection) {
		case 3:
			getdiff(1);
		case 2:
			getdiff(2);
		case 1:
			if (g) {
				delete g;
				g = nullptr;
			}
			g = new Game(diff1, diff2);
		case 4:
			g->GameStart();
			g->GameLoop();
			break;
		case 8:
			help();
			break;
		case 9:
			return;
		}
	}
};