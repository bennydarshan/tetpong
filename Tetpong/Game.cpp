#include "Game.h"

void Game::GameLoop()
{
	while (running)
	{
		ev.Poll();

		/*if (b1.hit(p1) | b1.hit(p2)) {
			b1.invertX();
		}*/

		if ((gdiff1) && (b1.hit(a1))) b1.invertX();
		else if (!gdiff1 && b1.hit(p1)) b1.invertX();
		if ((gdiff2) && b1.hit(a2)) b1.invertX();
		else if (!gdiff2 && b1.hit(p1)) b1.invertX();


		if (b1.isbomb())
		{
			int x = w1.bombhit(b1);
			if (x >= 0) w1.shift(x);
			x = w2.bombhit(b1);
			if (x >= 0) w2.shift(x);

		}
		if (gdiff1) a1.process(b1);
		if (gdiff2) a2.process(b1);

		if (!b1.move()) { 
			if (b1.DirX() == Point::LEFT) {
				w1.embed(p1.getY() - MIN_Y);
				int j = w1.query();
				if (j > -1) {
					w1.shift(j);
					p1.heal(5);
				}
				else
				{
					if (b1.isbomb()) for (int i = 0; i < 3; i++) p1.die();
					else
						p1.die();
					b1.setmX(p1.getDeaths());
				}
			}
			else {
				w2.embed(p2.getY() - MIN_Y);
				int j = w2.query();
				if (j > -1) {
					w2.shift(j);
					p2.heal(5);
				}
				else {
					if (b1.isbomb()) for (int i = 0; i < 3; i++) p2.die();
					else p2.die();
					b1.setMX(p2.getDeaths());
				}
			}
			b1.reset();
			b1.draw();
		}
		Sleep(1000/15);
	}
}
