#include "EvHandler.h"
#include <conio.h>

bool EvHandler::Poll()
{
	if (!_kbhit()) return false;
	char clicked = tolower(_getch());
	if (clicked == 27)
		for (auto sub : subs[26]) sub->handle(clicked);
	int i = clicked - 'a';
	if (i <= -1) return false;
	if (i > 25) return false;
	for (auto sub : subs[i]) sub->handle(clicked);
	return false;
}
