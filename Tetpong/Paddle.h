#pragma once

#include "Point.h"
#include "Subscriber.h"
#include <array>

class Paddle: public Point {
private:
	int deaths = 0;
public:
	Paddle(int x, int y, char ch): Point(x, y, 1, 3, ch){}
	void draw() const;
	bool move(Point::dir d);
	void die();
	bool lost()
	{
		return deaths == 20;
	}
	void heal(int health);
	int getDeaths()
	{
		return deaths;
	}
};